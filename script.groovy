def buildDockerImg() {
	echo 'building docker image....'
	sh 'docker build -t test-nodejs-app:1.1 .'
}

def deployApp() {
	echo 'deploying app....'
	sh 'docker stack deploy -c nodejsapp.yml test'
}

return this
