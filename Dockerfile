FROM node:12-alpine AS build
WORKDIR /app
COPY package*.json ./
RUN npm ci --only=production
COPY . .

FROM node:12-alpine
#FROM gcr.io/distroless/nodejs:12
WORKDIR /app
COPY --from=build /app ./
CMD ["node","src/app.js"]

